<?php

/*
Plugin Name: Beans Woo Env
Plugin URI: http://URI_Of_Page_Describing_Plugin_and_Updates
Description: A brief description of the Plugin.
Version: 1.0
Author: Beans
Author URI: https:/www.trybeansc.com
*/

namespace BeansWooEnv;

if ( ! defined('ABSPATH') ){
    exit;
}

class BeansWooEnv {
    public function init(){
        add_action('init', array(__CLASS__, 'set_env'));
        add_action( 'wp_print_scripts', array(__CLASS__, 'remove_woocommerce_password_strength'), 10 );

    }

    public static function remove_woocommerce_password_strength() {
        wp_dequeue_script( 'wc-password-strength-meter' );
    }

    public static function set_env(){
        putenv('BEANS_DOMAIN_API=api-3.bns.re');
        putenv('BEANS_DOMAIN_CONNECT=connect.bns.re');
        putenv('BEANS_DOMAIN_WWW=www.bns.re');
        putenv('BEANS_DOMAIN_STATIC=bnsre.s3.amazonaws.com');
        putenv('BEANS_DOMAIN_NAME=bns.re');
    }

    public static function remove_config(){

    }


}

(new BeansWooEnv())->init();



register_deactivation_hook(__FILE__, 'beans_woo_env_deactivation');